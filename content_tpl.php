<?php

foreach ($result as $data_element) {
?>
	<a href="<?= $data_element["link"] ?>" target="_blank" class="postElement <?= str_replace(" ", "",preg_replace("/[^A-Za-z0-9 ]/", "", $data_element["provider"])); ?>">
		<div class="post">
        	<h1><?= $data_element["title"] ?></h1>
        	<h2>via <?= $data_element["provider"] ?></h2>
        	<img src="<?= $data_element["imageUrl"] ?>">
      </div>
  	</a>
<?php
}

?>