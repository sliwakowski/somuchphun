<?php 
$con=mysqli_connect("localhost","phpmyadmin","2MZ3cOmS","vps");
$page = $_GET["p"];
$prev_page = 0;
$next_page = 1;
$limit = 20;
$offset = $page*$limit;
if($page==null || $page==0){
  $page=0;
}
else {
  $prev_page = $page-1;
  $next_page = $page+1;
}
$query = "SELECT * FROM somuchphun WHERE imageUrl IS NOT NULL AND imageUrl != '' ORDER BY id DESC LIMIT $limit OFFSET $offset";
$mysql_result = mysqli_query($con,$query);
while($row = $mysql_result->fetch_array())
{
  $result[] = $row;
}
$mysql_result->close();
$con->close();

$accounts = array();
foreach ($result as $element) {
  $account = $element["provider"];
  if(!in_array($account, $accounts)){
    array_push($accounts, $account);
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>So much phun</title>
  <link rel="stylesheet" type="text/css" href="styles.css" media="screen">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <link rel="icon" type="image/png" href="fav.png">
  <script type="text/javascript" src="jquery.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      $('.account_filter').click(function(e){
        var className = "."+$(this).attr("id");
        $(".account_filter").addClass("disabled");
        $(this).removeClass("disabled");
        if($(this).attr("id")=="all") {
          $(".postElement").show();
          $(".account_filter").removeClass("disabled");
        }
        else $(".postElement").hide();
        $(className).show();
      });
    });
  </script>
</head>
<body>
  <div id="container">

    <a href="#" id="logo">
      <img src="logo.png" />
    </a>
    <div id="filters">
      <?php
        foreach ($accounts as $single_account) {
          ?>
          <a target="_blank" class="account_filter" id="<?= str_replace(" ", "",preg_replace("/[^A-Za-z0-9 ]/", "", $single_account)); ?>">
            <div class="website">
              <?= $single_account ?>
            </div>
          </a>
          <?php
        }
      ?>
      <a target="_blank" class="account_filter" id="all">
            <div class="website">
              Everything
            </div>
          </a>
      <div id="note">
        We’re happy that you’re here. So... we’re aggregating phun pictures from sites listed above.
      </div>
    </div>
    <div id="content">

      <?php include("content_tpl.php"); ?>
      <div id="navigation">
        <a href="http://vps80787.ovh.net/somuchphun/index.php?p=<?= $prev_page ?>"> <div id="nav_prev">Previous</div> </a>
        <a href="http://vps80787.ovh.net/somuchphun/index.php?p=<?= $next_page ?>"> <div id="nav_next">Next</div> </a>
      </div>

     </div>

    

  </div>

</body>

